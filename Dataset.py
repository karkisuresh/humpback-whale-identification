import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import cv2
import os
import math
from sklearn.model_selection import StratifiedShuffleSplit
from preprocess import preprocess
from preprocess import augment

matplotlib.use('PS')


class Dataset:

    def __init__(self):
        self.data_path = os.getcwd().replace("humpback-whale-identification", "data")
        self.df, self.df_index, self.duplicates = self.load_data()

    @staticmethod
    def visualize(df):
        # print(encoding)
        # print(len(new_df))
        # print(len(unique_classes))
        label_count = df.Id.value_counts()
        # print(label_count)
        # plt.hist(label_count, bins=100)
        # plt.xlabel("number of samples")
        # plt.ylabel("number of classes")
        # plt.title("number of samples per class")
        # plt.show()
        return label_count

    def load_data(self):
        train_path = os.path.abspath(os.path.join(self.data_path, "train"))
        csv_path = os.path.abspath(os.path.join(self.data_path, "train.csv"))
        # use pandas to load data
        df = pd.read_csv(csv_path)
        # add path to data
        df['path'] = [os.path.join(train_path, whale) for whale in df['Image']]
        # print(df.Id.value_counts().head())
        no_new_whales = df['Id'] != 'new_whale'
        # remove new_whale from data
        new_df = df[no_new_whales]
        # print(new_df.Id.value_counts().head())
        labels_count = self.visualize(new_df)
        df_index = new_df.index.values
        # print(new_df.index)
        # print(df_index)
        duplicates = []
        for i, row in new_df.iterrows():
            if labels_count[row['Id']] <= 2:
                dup = [i]*math.ceil((2 - labels_count[row['Id']])/labels_count[row['Id']])
                duplicates.extend(dup)
        # print(new_df.iloc[10])
        df_index = np.concatenate([df_index, duplicates])
        return new_df, df_index, duplicates

    def get_split_data(self, aug=True):
        # randomly permuting the values
        df_index = self.df_index[np.random.RandomState(seed=100).permutation(self.df_index.shape[0])]
        # print(len(train_index))
        # split the data into training and validation set
        split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=500)
        print(df_index.shape)
        train_set = []
        val_set = []
        for train_idx, test_idx in split.split(df_index, np.zeros(df_index.shape)):
            train_set, val_set = df_index[train_idx], df_index[test_idx]
        print(len(train_set))
        print(len(val_set))
        # classes that need to be in val set
        class_index = []
        for d in set(self.duplicates):
            class_index.append(d)
        val_set = np.concatenate([val_set, class_index])
        print(len(val_set))
        # loc = new_df.index.get_loc(train_set[0])
        # print(rev_encoding[new_df.iloc[loc].Id])
        # print(new_df.iloc[loc].Image)
        # save train data
        train_x, train_y = self.save(train_set, aug=aug)
        # save test data
        test_x, test_y = self.save(val_set, True)
        return train_x, train_y, test_x, test_y

    def get_all_data(self):
        test_path = os.path.abspath(os.path.join(self.data_path, "test"))
        test = os.listdir(test_path)
        col = ['Image']
        test_df = pd.DataFrame(test, columns=col)
        test_df['path'] = [os.path.join(test_path, whale) for whale in test_df['Image']]
        test_x = []
        for img_path in test_df['path']:
            img = cv2.imread(img_path)
            img = preprocess(img)
            test_x.append(img)
        train_x, train_y = self.save(self.df_index, aug=True)
        return train_x, train_y, np.array(test_x), test_df

    def save(self, index_set, test=False, aug=True):
        x = []
        y = []
        for index in index_set:
            loc = self.df.index.get_loc(index)
            value = self.df.iloc[loc]
            img_path = value.path
            label = value.Id
            img = cv2.imread(img_path)
            img = preprocess(img)
            if not test:
                if aug:
                    aug_images = augment(img)
                    for image in aug_images:
                        # plt.imshow(image, cmap='gray')
                        # plt.show()
                        x.append(image)
                        y.append(label)
                else:
                    x.append(img)
                    y.append(label)
            else:
                x.append(img)
                y.append(label)
            print(len(y)/4)
        return np.array(x), np.array(y)

    def visual(self):
        random_samples = np.random.choice(self.df['path'], 4)
        fig = plt.figure()
        for idx, path in enumerate(random_samples):
            fig.add_subplot(2, 2, idx+1)
            img = cv2.imread(path)
            plt.imshow(img)
        plt.show()
        print(random_samples)
