import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import cv2
import gc
import ml_metrics as metrics
from keras.layers import Dense, Activation, BatchNormalization, Flatten, Conv2D
from keras.layers import AveragePooling2D, MaxPooling2D, Dropout
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from Dataset import Dataset
from keras.applications.resnet50 import ResNet50
from preprocess import preprocess

from keras.models import Sequential, load_model, Model

import warnings
warnings.simplefilter("ignore", category=DeprecationWarning)

# matplotlib.use('PS')
matplotlib.use('TkAgg')


def model1(train_y):
    model = Sequential()
    model.add(Conv2D(32, (7, 7),
                     strides=(1, 1),
                     name='conv0',
                     input_shape=(128, 128, 1)))
    model.add(BatchNormalization(axis=3, name='bn0'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), name='max_pool'))
    model.add(Conv2D(64, (3, 3), strides=(1, 1), name="conv1"))
    model.add(Activation('relu'))
    model.add(AveragePooling2D((3, 3), name='avg_pool'))

    model.add(Flatten())
    model.add(Dense(500, activation="relu", name='rl'))
    model.add(Dropout(0.8))
    model.add(Dense(train_y.shape[1], activation='softmax', name='sm'))
    return model


def simple(train_y):
    model = Sequential()
    model.add(Conv2D(32, (5, 5),
                     strides=(1, 1),
                     name='conv0',
                     input_shape=(128, 128, 1)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D((2, 2), name='max_pool'))
    model.add(Flatten())
    model.add(Dropout(0.8))
    model.add(Dense(train_y.shape[1], activation='softmax', name='sm'))
    return model


def encode_label(y):
    label_encoder = LabelEncoder()
    int_encoded = label_encoder.fit_transform(y)
    # print(int_encoded)

    one_hot_encoder = OneHotEncoder(sparse=False)
    int_encoded = int_encoded.reshape(len(int_encoded), 1)
    one_hot_encoded = one_hot_encoder.fit_transform(int_encoded)
    # print(one_hot_encoded)

    y = one_hot_encoded
    print(y.shape)
    return y, label_encoder


def train_and_validate(data):
    train_x, train_y, test_x, test_y = data.get_split_data()
    n, x, y, c = train_x.shape
    train_y, encoder = encode_label(train_y)
    train_x = np.reshape(train_x, (n, x, y, c))
    print(train_x.shape)
    print(train_y.shape)
    # print(train_y)
    test_x = np.reshape(test_x, (len(test_x), x, y, c))
    # test_y, test_encoder = encode_label(test_y)

    print(train_y.shape)
    # print(test_y.shape)

    # build model
    # mod = model1(train_y)
    mod = ResNet50(weights=None, classes=train_y.shape[1])
    mod.compile(loss='categorical_crossentropy', optimizer="adam", metrics=['accuracy'])
    print(mod.summary())

    history = mod.fit(train_x, train_y, epochs=200, batch_size=100, verbose=1)
    gc.collect()
    pred = mod.predict(test_x, verbose=1)
    y_pred = []
    for p in pred:
        # print("p: " + str(p.argsort()[-5:][::-1]))
        y_pred.append(encoder.inverse_transform(p.argsort()[-5:][::-1]))
    # print(test_y)
    y_pred = np.array(y_pred)
    # print(y_pred)
    print(metrics.mapk(test_y, y_pred, 5))


def train_and_test(data):
    train_x, train_y, test_x, test_df = data.get_all_data()
    # n, x, y = train_x.shape
    train_y, encoder = encode_label(train_y)
    # train_x = np.reshape(train_x, (n, x, y, 1))
    n, x, y = test_x.shape
    test_x = np.reshape(test_x, (len(test_x), x, y, 1))
    # mod = model1(train_y)
    mod = ResNet50(weights=None, classes=train_y.shape[1])
    mod.compile(loss='categorical_crossentropy', optimizer="adam", metrics=['accuracy'])
    print(mod.summary())

    history = mod.fit(train_x, train_y, epochs=200, batch_size=100, verbose=1)
    gc.collect()
    mod.save('cnn_model.h5')
    mod = load_model('cnn_model.h5')
    pred = mod.predict(test_x, verbose=1)
    threshold = 0.5
    for i, p in enumerate(pred):
        # print("p: " + str(p.argsort()[-5:][::-1]))
        if max(p) < threshold:
            result = ['new_whale']
            result.extend(encoder.inverse_transform(p.argsort()[-4:][::-1]))
            test_df.loc[i, 'Id'] = ' '.join(result)
        else:
            test_df.loc[i, 'Id'] = ' '.join(encoder.inverse_transform(p.argsort()[-5:][::-1]))
    test_df = test_df.drop('path', axis=1)
    print(test_df.head(10))
    test_df.to_csv('submission.csv', index=False)


def visualize(data):
    img = cv2.imread("D:\\College\\ML\\Project\\data\\train\\02a34f2bd.jpg")
    img = preprocess(img)
    plt.interactive(False)
    plt.imshow(img, cmap='gray')
    plt.show(block=True)
    model = load_model('cnn_model.h5')
    layer_outputs = [layer.output for layer in model.layers]
    activation_model = Model(inputs=model.input, outputs=layer_outputs)
    activations = activation_model.predict(img.reshape(1, 128, 128, 1))
    act_index = 3
    activation = activations[act_index]
    activation_index = 1
    row_size = 5
    col_size = 5
    fig, ax = plt.subplots(row_size, col_size, figsize=(row_size * 2.5, col_size * 1.5))
    for row in range(0, row_size):
        for col in range(0, col_size):
            ax[row][col].imshow(activation[0, :, :, activation_index])
            activation_index += 1
    plt.show()


if __name__ == '__main__':
    data = Dataset()
    # train_and_validate(data)
    # train_and_test(data)
    visualize(data)


