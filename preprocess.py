import cv2
import numpy as np
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import skimage as sk
from skimage import transform
from skimage import util


def preprocess(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    res = cv2.resize(img, dsize=(128, 128), interpolation=cv2.INTER_CUBIC)
    # histogram equalization
    img = cv2.equalizeHist(res)
    return img
    # return res


def augment(img):
    images = list()
    images.append(img)
    augs = [rotate, add_noise, flip]
    for aug in augs:
        images.append(aug(img))
    # fig = plt.figure()
    # for idx, img in enumerate(images):
    #     fig.add_subplot(2, 2, idx+1)
    #     plt.imshow(img)
    # plt.show()
    return images


def rotate(img):
    deg = np.random.uniform(-50, 50)
    return sk.transform.rotate(img, deg)


def add_noise(img):
    return sk.util.random_noise(img)


def flip(img):
    return img[:, ::-1]


